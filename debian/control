Source: r-cran-hdf5r
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-hdf5r
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-hdf5r.git
Homepage: https://cran.r-project.org/package=hdf5r
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-r6,
               r-cran-bit64,
               libhdf5-dev
Testsuite: autopkgtest-pkg-r

Package: r-cran-hdf5r
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R interface to the 'HDF5' binary data format
 'HDF5' is a data model, library and file format for storing
 and managing large amounts of data. This package provides a nearly
 feature complete, object oriented  wrapper for the 'HDF5' API
 <https://support.hdfgroup.org/HDF5/doc/RM/RM_H5Front.html> using R6 classes.
 Additionally, functionality is added so that 'HDF5' objects behave very
 similar to their corresponding R counterparts.
